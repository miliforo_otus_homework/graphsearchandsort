
#include <iostream>
#include <queue>
#include <vector>

using namespace std;

// vector<vector<int>> Matrix{
//     {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0}, //1
//     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0}, //2
//     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //3
//     {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //4
//     {0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0}, //5
//     {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0}, //6
//     {0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0}, //7
//     {0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1}, //8
//     {1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0}, //9
//     {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1}, //10
//     {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //11
//     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //12
//     {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //13
//     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0} //14
//     // 1  2  3  4  5  6  7  8  9 10  11 12 13 14  
// };

vector<vector<int>> Matrix{
        {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0}, //0
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0}, //1
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //2
        {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //3
        {0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0}, //4
        {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0}, //5
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0}, //6
        {0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0}, //7
        {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}, //8
        {1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0}, //9
        {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //10
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //11
        {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //12
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} //13
    // 0  1  2  3  4  5  6  7  8  9  10 11 12 13
    };



int GetSumOfColumns(vector<vector<int>> Matrix, int Index)
{
    int Sum = 0;
    for (int j = 0; j < Matrix.size(); j++)
    {
        Sum+=Matrix.at(j).at(Index);
    }
    return Sum;
}


void Recount(vector<vector<int>> OriginalMatrix, vector<int>& CurrentMatrix, int Index)
{
    for (int i = 0; i < OriginalMatrix.size(); i++)
        CurrentMatrix[i] -= Matrix[Index].at(i);
}

bool MatrixSort(vector<vector<int>>& Matrix)
{
    
    int Level = 0;
    vector<vector<int>> Order;
    Order.resize(Matrix.size());
    
    vector<int> SumOfColumns;
    vector<int> Vertexes; 
    
    for (int i = 0; i < Matrix.size(); i++)
    {
        SumOfColumns.push_back(GetSumOfColumns(Matrix, i));
        Vertexes.push_back(i);
    }
    
    while (!Vertexes.empty())
    {
        vector<int> Indexes; 
        for (int i : Vertexes)
        {
            if(SumOfColumns[i] == 0)
            {
                Indexes.push_back(i);
            }
        }

        if (Indexes.empty())
        {
            return false; 
        }

        for (int i : Indexes)
        {
            Order[Level].push_back(i);
            Vertexes.erase(remove(Vertexes.begin(), Vertexes.end(), i), Vertexes.end());
            Recount(Matrix, SumOfColumns, i); 
        }
        Level++; 
    }

    for (int i = 0; i < Order.size(); i++)
        for (int j = 0; j < Order[i].size(); j++)
        cout << "Level " << i << " Vertex " << Order[i][j] << "\n";

    
    return true; 
}

int main(int argc, char* argv[])
{

  MatrixSort(Matrix);
    
    return 0;
}
